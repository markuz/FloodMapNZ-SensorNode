// {{{ Includes ---------------------------------------------------------------
#include <Arduino.h>
#include <elapsedMillis.h>
#include <avr/pgmspace.h>
#include <lmic.h>
#include <hal/hal.h>
#include <SPI.h>
#include <RTCZero.h>
#include "dtostrf.h"
#include "configuration.h"
// ------------------------------------------------------------------------ }}}
// {{{ Configuration ----------------------------------------------------------
// {{{ RTC
RTCZero rtc;
// }}}
// {{{ LoRaWAN
// Feather M0 RFM9x pin mappings - DO NOT CHANGE!
lmic_pinmap pins = {
  .nss = 8,         // Internal connected
  .rxen = 0,        // Not used for RFM92/RFM95
  .txen = 0,        // Not used for RFM92/RFM95
  .rst = 4,         // Internal connected
  .dio = {3, 5, 6}, // Connect "i01" to "5"
                    // Connect "D2" to "6"
};
// Check to see if the data has been sent
bool dataSent = false;
// }}}
// {{{ HC-SR04+
#if defined(SENSOR_SR04PLUS)
#include <NewPing.h>
NewPing sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE); // NewPing setup of pins and maximum distance.
#endif
// }}}
// ------------------------------------------------------------------------ }}}
// {{{ Setup  &  Loop ---------------------------------------------------------
// {{{ void setup()
/**
 * Device Start Up
 */
void setup() {

    #if defined(NODE_DEBUG)
    Serial.begin(115200);
    delay(1000);
    Serial.println("Starting...");
    #endif

    // RTC
    rtc.begin();

    #if defined(UPDATE_MINUTELY)
    rtc.setAlarmSeconds(0);
    rtc.enableAlarm(rtc.MATCH_SS);
    #endif

    #if defined(UPDATE_HOURLY)
    rtc.setAlarmMinutes(0);
    rtc.setAlarmSeconds(0);
    rtc.enableAlarm(rtc.MATCH_MMSS);
    #endif

    #if defined(UPDATE_DAILY)
    rtc.setAlarmTime(0, 0, 0);
    rtc.enableAlarm(rtc.MATCH_HHMMSS);
    #endif

    rtc.attachInterrupt(alarmMatch);

    // Setup LoRaWAN state
    initLoRaWAN();

    // Shutdown the radio
    os_radio(RADIO_RST);

    #if defined(NODE_DEBUG)
    Serial.println("Startup complete");
    #endif

    // Give it a break
    delay(16000);

}
// }}}
// {{{ void loop()
/**
 * Main loop - send data and stand by until the next alarm
 *
 * Full Packet:
 *
 * b:N.NN|d:NNN|m:123456
 *
 */
void loop() {

    char packet[32] = "";
    char delimiter[1] = "|";

    #if defined(NODE_DEBUG)
    Serial.println("\nBeginning to send data");
    #endif

    #if defined(BATTERY_STATUS)
    char *battery = getBattery();
    strcat(packet, battery);
    #if defined(SENSOR_SR04PLUS) || defined(NODE_DEBUG)
    strcat(packet, delimiter);
    #endif
    #endif

    #if defined(SENSOR_SR04PLUS)
    char *distance = getDistance();
    strcat(packet, distance);
    #if defined(NODE_DEBUG)
    strcat(packet, delimiter);
    #endif
    #endif

    #if defined(NODE_DEBUG)
    char *freemem = getFreeMem();
    strcat(packet, freemem);
    #endif

    #if defined(BATTERY_STATUS) || defined(SENSOR_SR04PLUS) || defined(NODE_DEBUG)
    sendData(packet);
    #endif

    // Shutdown the radio
    os_radio(RADIO_RST);

    #if defined(NODE_DEBUG)
    Serial.println("\nAll data sent");
    #endif

    rtc.standbyMode();

}
// }}}
// ------------------------------------------------------------------------ }}}
// {{{ Node Functions ---------------------------------------------------------
// {{{ void alarmMatch()
void alarmMatch() {
}
// }}}
// {{{ void initLoRaWAN()
void initLoRaWAN() {

    // LMIC init
    os_init();

    // Reset the MAC state. Session and pending data transfers will be discarded.
    LMIC_reset();

    // by joining the network, precomputed session parameters are be provided.
    LMIC_setSession(0x1, DevAddr, (uint8_t*)NwkSkey, (uint8_t*)AppSkey);

    // Enabled data rate adaptation
    LMIC_setAdrMode(1);

    // Enable link check validation
    LMIC_setLinkCheckMode(0);

    // Set data rate and transmit power
    LMIC_setDrTxpow(DR_SF12, 21);

}
// }}} 
// {{{ char *getDistance()
#if defined(SENSOR_SR04PLUS)
/**
 * Send a message with the distance.
 */
char *getDistance() {
    int distance = sonar.ping_cm();
    char intStr[5];
    itoa(distance, intStr, 10);
    char packet[7] = "d:";
    strcat(packet, intStr);
    return packet
}
#endif
// }}}
// {{{ char *getFreeMem()
#if defined(NODE_DEBUG)
/**
 * Send a message with the free memory.
 */
extern "C" char *sbrk(int i);
char *getFreeMem() {
    char stack_dummy = 0;
    int freeMem = &stack_dummy - sbrk(0);
    char intStr[10];
    itoa(freeMem, intStr, 10);
    char packet[12] = "m:";
    strcat(packet, intStr);
    return packet
}
#endif
// }}}
// {{{ char *getBattery()
#if defined(BATTERY_STATUS)
/**
 * Send a message with the battery voltage.
 */
char *getBattery() {
    float measuredvbat = analogRead(A7); // Hard wired to pin A7 on Adafruit Feather
    measuredvbat *= 2;                   // we divided by 2, so multiply back
    measuredvbat *= 3.3;                 // Multiply by 3.3V, our reference voltage
    measuredvbat /= 1024;                // convert to voltage
    char floatStr[8];
    dtostrf(measuredvbat, 3, 2, floatStr);
    char packet[10] = "b:";
    strcat(packet, floatStr);
    return packet;
}
#endif
// }}}
// {{{ void sendData(char *packet)
/**
 * Send data packet.
 */
void sendData(char *packet) {

    // Ensure there is not a current TX/RX job running
    if (LMIC.opmode & (1 << 7)) {
        // Something already in the queque
        return;
    }

    #if defined(NODE_DEBUG)
    Serial.print("  seqno ");
    Serial.print(LMIC.seqnoUp);
    Serial.print(": ");
    Serial.println(packet);
    #endif

    // Add to the queque
    dataSent = false;
    uint8_t lmic_packet[32];
    strcpy((char *)lmic_packet, packet);
    LMIC_setTxData2(1, lmic_packet, strlen((char *)lmic_packet), 0);

    // Wait for the data to send or timeout after 15s
    elapsedMillis sinceSend = 0;
    while (!dataSent && sinceSend < 15000) {
        os_runloop_once();
        delay(1);
    }
    os_runloop_once();
}
// }}}
// ------------------------------------------------------------------------ }}}
// {{{ LMIC Callbacks ---------------------------------------------------------
// {{{ void os_getArtEui (u1_t* buf)
/**
 * Get the AppEUI callback.
 */
void os_getArtEui (u1_t* buf) {
    memcpy(buf, AppEui, 8);
}
// }}}
// {{{ void os_getDevKey (u1_t* buf)
/**
 * Get the Network Session Key callback.
 */
void os_getDevKey (u1_t* buf) {
    memcpy(buf, NwkSkey, 16);
}
// }}}
// {{{ void onEvent (ev_t ev)
/**
 * On a LMIC event callback.
 */
void onEvent (ev_t ev) {
    if (ev == EV_TXCOMPLETE) {
        dataSent = true;
    }
    if (ev == EV_LINK_DEAD) {
        initLoRaWAN();
    }
}
// }}}
// ------------------------------------------------------------------------ }}}
